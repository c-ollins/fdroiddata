Categories:Money
License:MIT
Web Site:
Source Code:https://github.com/C-ollins/Simple-Decred-Widget
Issue Tracker:https://github.com/C-ollins/Simple-Decred-Widget/issues

Auto Name:Simple Decred Widget
Summary:A Simple Decred Widget
Description:
A simple widget for viewing Decred exchange rate, as well as PoS Ticket price,
mining difficulty, and more
.

Repo Type:git
Repo:https://github.com/C-ollins/Simple-Decred-Widget

Build:3.0,3
    commit=e716932f7c8757f9c6e23aef98751cd949223aed
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:3.0
Current Version Code:3
